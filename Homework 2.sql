drop table if exists user1, settings cascade;
create table user1
(
    first_name text,
    last_name text,
    nickname text primary key
);
create table settings
(
    font_size       int,
    color_scheme       text,
    user_nickname        text references user1
);

insert into user1
values ('Mary', 'Kay', 'Flower'),
       ('John', 'Morty', 'Baby'),
       ('Albert', 'Smith', 'Mister');
-- значения вставляемые в столбец holder_phone должны присутствовать в главной таблице в столбце phone
insert into settings (font_size, color_scheme, user_nickname)
values (14, 'Blue', 'Baby'),
       (26, 'Red', 'Flower'),
       (38, 'Black', 'Mister');
-- соединение таблиц по ключу и вывод всех колонок
select *
from user1
         join settings on nickname = user_nickname;
-- соединение таблиц по ключу и вывод определённых колонок
select nickname, first_name, last_name as settings_font_size, color_scheme
from user1
         join settings on nickname = user_nickname;