-- таблица связи много-много

DROP TABLE IF exists holder, equipment, equipment_to_holder CASCADE;

CREATE TABLE holder
(
 name text,
 phone text PRIMARY KEY
);

CREATE TABLE equipment
(
 title text PRIMARY key,
 color text
);

CREATE TABLE equipment_to_holder
(
 holder_phone text REFERENCES holder,
 equipment_title text REFERENCES equipment
);

insert into holder
values ('Альберт Андреевич', '0001'),
       ('Иван Вячеславович', '0002'),
       ('Вячеслав Александрович', '0003');

insert into equipment (title, color)
values ('Ракетка', 'Рыбный'),
       ('Мяч', 'Серый'),
       ('Палатка', 'Аквамариновый'),
       ('Удочка', 'Апельсиновый'),
       ('Мангал', 'Пурпурный'),
       ('Спальник', 'Коричневый'),
       ('Рюкзак', 'Синий'),
       ('Дождевик', 'Желтый'),
       ('Компас', 'Красный'),
       ('Термос', 'Синий');
      
 insert into equipment_to_holder
 VALUES ('0001','Ракетка'),
   ('0001','Мяч');
  
 insert into equipment_to_holder
 VALUES ('0002','Спальник'),
   ('0002','Мяч');
 
select name, holder_phone, equipment_title, color 
from holder h 
 join equipment_to_holder eth on h.phone = eth.holder_phone 
 join equipment e on eth.equipment_title  = e.title;