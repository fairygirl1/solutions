drop table if exists tour, review cascade;
create table tour
(
    name  text,
    price numeric,
    id int primary key
);
create table review
(
    comment        text,
    rating       int,
    tour_id        int references tour
);
-- вставка данных
insert into tour
values ('Абхазия', 3200.6, 1),
       ('Сочи', 5600, 2),
       ('Шепси', 900, 3);
-- значения вставляемые в столбец holder_phone должны присутствовать в главной таблице в столбце phone
insert into review (comment, rating, tour_id)
values ('good', 1, 3),
       ('like it', 3, 1),
       ('perfect', 1, 3),
       ('the worst trip', 3, 1),
       ('quiet good', 2, 2),
       ('the beat', 1, 1);
-- соединение таблиц по ключу и вывод всех колонок
select *
from tour
         join review on id = tour_id;
-- соединение таблиц по ключу и вывод определённых колонок
select name, price, id as review_comment, rating
from tour
         join review on id = tour_id
order by rating;