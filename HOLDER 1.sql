-- таблица связи один - много
-- удаление таблиц, если они уже созданы
drop table if exists holder, equipment cascade;
-- главная таблица
create table holder
(
    name  text,
    phone text
);
-- зависимая таблица
create table equipment
(
    title        text,
    count        int,
    holder_phone text -- столбец, через который мы ссылаемся на главную таблицу
);
-- для задания ссылочной целостности задается первичный ключ (уникальный идентификатор строки) в главной таблице
alter table holder
    add primary key (phone);
-- и внешний ключ в зависимой таблице
alter table equipment
    add foreign key (holder_phone) references holder;
drop table if exists holder, equipment cascade;
-- более краткая форма записи, в дальнейшем нужно(!!!) использовать её
create table holder
(
    name  text,
    phone text primary key
);
create table equipment
(
    title        text,
    count        int,
    holder_phone text references holder
);
-- вставка данных
insert into holder
values ('Альберт Андреевич', '0001'),
       ('Иван Вячеславович', '0002'),
       ('Вячеслав Александрович', '0003');
-- значения вставляемые в столбец holder_phone должны присутствовать в главной таблице в столбце phone
insert into equipment (title, count, holder_phone)
values ('Ракетка', 2, '0003'),
       ('Мяч', 3, '0001'),
       ('Палатка', 6, '0003'),
       ('Удочка', 2, '0001'),
       ('Мангал', 1, '0002'),
       ('Спальник', 5, '0001'),
       ('Рюкзак', 10, '0002'),
       ('Дождевик', 10, '0002'),
       ('Компас', 1, '0003'),
       ('Термос', 4, '0001'),
       ('Перчатка', 11, '0001');
-- соединение таблиц по ключу и вывод всех колонок
select *
from holder
         join equipment on phone = holder_phone;
-- соединение таблиц по ключу и вывод определённых колонок
select name, phone, title as equipment_title, count
from holder
         join equipment on phone = holder_phone;